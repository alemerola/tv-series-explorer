import React, { Component } from 'react';
import Main from '../Main';
import './Header.css';

class Header extends Component {
  render() {
    return (
        <div>
            <div className="App">
                <header className="App-header">
                    <img src="https://www.ilovenoname.com/wp-content/uploads/2017/11/dreamstime_s_42674350.jpg" className="App-logo" alt="logo" />
                    <span className="App-title">Tv Series Explorer</span>
                </header>
            </div>
            <div>
                <Main/>
            </div>
        </div>
    );
  }
}

export default Header;
