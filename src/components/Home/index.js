import React, { Component } from 'react';
import ResultsContainer from '../ResultsContainer';
import SearchContainer from '../SearchContainer';
import './Home.css';
import {Row, Col} from 'react-materialize'

class Home extends Component {
    state = {
      show: null
    };

    onShowSelected = (show) => {
        console.log(show);
        this.setState({show: show})
    };


    render() {
        return (
            <div className="padding">
                <Row>
                    <Col m={4} s={12}>
                        <SearchContainer onShowSelected={this.onShowSelected} />
                    </Col>
                    <Col offset="m2" m={6} s={12}>
                        {this.state.show && <ResultsContainer show={this.state.show} />}
                    </Col>
                </Row>
            </div>
        );
    }
}

export default Home;
