import React from 'react';
import Home from '../../components/Home'
import { Switch, Route } from 'react-router-dom';

const Main = props =>
                    <Switch>
                        <Route exact path="/" component={Home} />
                    </Switch>;

export default Main;