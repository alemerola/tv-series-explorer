import React from 'react';
import renderHTML from 'react-render-html';
import { Card, Row, Col, CardTitle } from 'react-materialize';
import './ResultsContainer.css';

const ResultsContainer = ({show}) =>
    <div>
        <Row>
            <Col s={12}>
                <Card horizontal header={<CardTitle image={show.image && show.image.medium}></CardTitle>}>
                    <table>
                        <tbody>
                        <tr>
                            <td>Genres: </td>
                            <td>{show.genres && show.genres.map((g) => `${g}, `)}</td>
                        </tr>
                        <tr>
                            <td>Network: </td>
                            <td>{show.network && show.network.name}</td>
                        </tr>
                        <tr>
                            <td>Premiere: </td>
                            <td>{show.premiered}</td>
                        </tr>
                        <tr>
                            <td>Runtime: </td>
                            <td>{show.runtime}"</td>
                        </tr>
                        <tr>
                            <td>Status: </td>
                            <td>{show.status}</td>
                        </tr>
                        </tbody>
                    </table>
                </Card>
            </Col>
            <Col s={12}>
                <Card title={<h4>SUMMARY</h4>}>
                    <div>
                        {renderHTML(show.summary)}
                    </div>
                </Card>
            </Col>
        </Row>
    </div>;


export default ResultsContainer;
