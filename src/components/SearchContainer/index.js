import React, { Component } from 'react';
import './SearchContainer.css';
import { Table } from 'react-materialize';
import { fromEvent } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

const SeriesItem = ({series, onClickSeries}) =>
                        <tr onClick={() => onClickSeries(series.show)}>
                            <td>{series.show.name}</td>
                            <td>{series.show.premiered && series.show.premiered.split('-')[0]}</td>
                        </tr>;


class SearchContainer extends Component {
    state = {
        series: []
    };

    searchBox$;
    searchBox;

    componentDidMount(){
        this.searchBox = document.querySelector('#search-box');
        this.searchBox$ = fromEvent(this.searchBox, 'input').pipe(
            map((e) => e.target.value),
            filter(text => text.length > 2),
            debounceTime(500),
            distinctUntilChanged(),
            switchMap((searchTerm) => ajax(`http://api.tvmaze.com/search/shows?q=${searchTerm}`))
        );
        this.searchBox$.subscribe(({response}) => this.setState({series: response}));
    }

  render() {
    return (
      <div>
          <input type="text" id="search-box" className="input-field col s6" placeholder="Type the series name"/>
          {
              this.state.series.length > 0 &&
              <Table striped>
                  <thead>
                    <tr>
                        <th>Name</th>
                        <th>Premiered</th>
                    </tr>
                  </thead>
                  <tbody>
                  {
                      this.state.series.map(
                          (series) =>
                              <SeriesItem series={series} onClickSeries={this.props.onShowSelected} key={series.show.id}/>
                      )
                  }
                  </tbody>
             </Table>
          }
      </div>
    );
  }
}

export default SearchContainer;
